# C3

Internal for the C3 project.

## Building

 * Run `cargo clean` if upgrading from a previous version of LLVM.
 * Build [LLVM 4 or 5 and Clang](http://releases.llvm.org/download.html) from source (`libclang.a` is needed, and pre-built packages won't have it).
 * Add directory containing `llvm-config` to your `PATH`, or set `LLVM_CONFIG_PATH` env variable poiting to the `llvm-config` executable file.
 * Set `LIBCLANG_INCLUDE_PATH` pointing to Clang's include directory (`<clang install dir>/clang/include/`)

### Building clang (Linux and macOS)

This will require 20GB of disk space and 16GB of RAM. This method creates static binaries, which can be redistributed without hassle.

1. Install cmake, subversion, build-essential, libffi-dev, libncursesw5-dev

   ```bash
   # get LLVM without clang
   curl -L http://releases.llvm.org/5.0.2/llvm-5.0.2.src.tar.xz | tar xz
   # get clang
   curl -L http://releases.llvm.org/5.0.2/cfe-5.0.2.src.tar.xz | tar xz
   # combine both
   mv cfe-5.0.2.src llvm-5.0.2.src/tools/clang
   cd llvm-5.0.2.src
   mkdir build
   cd build
   cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=$HOME/llvm-5-c3 -DLIBCLANG_BUILD_STATIC=ON -DLLVM_BUILD_LLVM_DYLIB=OFF -DLLVM_TARGETS_TO_BUILD=X86 -DCMAKE_BUILD_TYPE=MinSizeRel -DLLVM_POLLY_BUILD=OFF -DLLVM_ENABLE_LIBCXX=ON -DCMAKE_BUILD_TYPE=Release -DLLVM_BUILD_RUNTIME=OFF -DLLVM_ENABLE_TERMINFO=OFF -DLLVM_ENABLE_LIBEDIT=OFF -DLLVM_ENABLE_ZLIB=OFF -DLLVM_ENABLE_FFI=OFF ..
   make -j10 # Adjust j to your number of cores. Take a nap.
   make install
   cp lib/libclang.a "$HOME/llvm-5-c3/lib/"
   ```

2. Set the following variables. Note that you will need to **set them every time you compile**, so add them to your bashrc or make a script and source it.
    ```bash
    export LIBCLANG_INCLUDE_PATH="$HOME/llvm-5-c3/tools/clang/include/:$HOME/llvm-5-c3/include/"
    export LIBCLANG_STATIC_PATH="$HOME/llvm-5-c3/lib/"
    export LLVM_CONFIG_PATH="$HOME/llvm-5-c3/bin/llvm-config"
    ```
